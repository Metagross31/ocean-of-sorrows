TeleporterRange = 15

State({
    StateName = "INIT",
    OnOneTimeEvent({
        Conditions = {},
        Actions = {
            BuildingModeSetEnabled({
                Tag = "tp_switch_1"
            }),
            EntityFlagSetTrue({
                Name = "ef_TeleporterIsEnabled"
            })
        }
    }),

    OnEvent({
        Conditions = {
            EntityFlagIsTrue({
                Name = "ef_TeleporterIsEnabled"
            }),
            EntityHasJustCastedSpell({ -- This checks if the ability has been used
                Tag = "tp_switch_1",
                SpellId = 2219
            }),
            TeamSquadOnGroundIsInRange({ -- Checks for range again
                Team = "tm_Team1",
                TargetTag = "tp_start_1",
                Range = TeleporterRange
            })
        },

        Actions = {
            TeamSquadTeleportInRange({ -- Teleports player units
                Tag = "tp_start_1",
                Range = TeleporterRange,
                TargetTag = "tp_target_1",
                Team = "tm_Team1"
            }),
            TeamSquadTeleportInRange({-- Teleports enemy units
                Tag = "tp_start_1",
                Range = TeleporterRange,
                TargetTag = "tp_target_1",
                Team = "tm_Team2"
            })
        }
    }),

    OnEvent({
        Conditions = {
            EntityFlagIsTrue({
                Name = "ef_TeleporterIsEnabled"
            }),
            EntityHasJustCastedSpell({
                Tag = "tp_switch_1",
                SpellId = 2219
            }),
            TeamSquadIsNotInRange({
                Team = "tm_Team1",
                TargetTag = "tp_start_1",
                Range = TeleporterRange
            })
        },
        Actions = {
            TeamSquadTeleportInRange({
                Tag = "tp_start_1",
                Range = TeleporterRange,
                TargetTag = "tp_target_1",
                Team = "tm_Team2"
            })
        }
    })
})
