State({
    StateName = "WAGON",
    OnOneTimeEvent({
        Conditions = {
            TeamSquadIsInRange({
                Team = "tm_Team1",
                TargetTag = "goldwagon",
                Range = 7
            })
        },
        Actions = {
            MapTimerStart({
                Name = "mt_wagon_p1"
            })
        }
    }),
    OnOneTimeEvent({
        Conditions = {
            TeamSquadIsInRange({
                Team = "tm_Team2",
                TargetTag = "goldwagon",
                Range = 7
            })
        },
        Actions = {
            MapTimerStart({
                Name = "mt_wagon_p2"
            })
        }
    }),
    OnOneTimeEvent({
        Conditions = {
            TeamSquadIsInRange({
                Team = "tm_Team1",
                TargetTag = "goldwagon",
                Range = 7
            }),
            MapTimerIsElapsed({
                Name = "mt_wagon_p1",
                Seconds = 20,
            })
        },
        Actions = {
            MapTimerStop({
                Name = "mt_wagon_p1"
            }),
            PlayerPowerGive({
                Player = "pl_Player1",
                Amount = 50
            })
        }
    }),
    OnOneTimeEvent({
        Conditions = {
            TeamSquadIsInRange({
                Team = "tm_Team2",
                TargetTag = "goldwagon",
                Range = 7
            }),
            MapTimerIsElapsed({
                Name = "mt_wagon_p2",
                Seconds = 20,
            })
        },
        Actions = {
            MapTimerStop({
                Name = "mt_wagon_p2"
            }),
            PlayerPowerGive({
                Player = "pl_Player2",
                Amount = 50
            })
        }
    })
})
