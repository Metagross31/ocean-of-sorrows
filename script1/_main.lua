State({
    StateName = "INIT",
    OnOneTimeEvent({
        EventName = "Initiate Timers",
		Conditions = {},
        Actions = {
            MapTimerStart({
                Name = "mt_1stspitfiretimer_p1"
            }),
            MapTimerStart({
                Name = "mt_1stspitfiretimer_p2"
            })
        }
    }),
    OnOneTimeEvent({
        EventName = "Set Battleship Spawners",
        Conditions = {},
        Actions = {
            EntitySpellAdd({Tag = "spawn_battleship_ne", SpellId = 1931}),
            EntitySpellAdd({Tag = "spawn_battleship_sw", SpellId = 1931}),
            MissionOutcry {Player = "ALL", TextTag = "", PortraitFileName = "unit_RavenCaptain_outcry", DurationSeconds = 10, Text = "Spawners initialized"},
        }
    })
})
