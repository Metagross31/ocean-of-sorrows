State({
    StateName = "SPITFIRESPLAYERTWO",
    OnOneTimeEvent({
        EventName = "Init",
        Conditions = {},
        Actions = {
            EntityValueSet({
                Name = "ev_spitfirecounter_p2",
                Value = 0
            })
        }
    }),
    OnOneTimeEvent({
        EventName = "1stSpitfire_p2",
        Conditions = {
            MapTimerIsElapsed({
                Name = "mt_1stspitfiretimer_p2",
                Seconds = 13
            })
        },
        Actions = {
            MapTimerStop({
                Name = "mt_1stspitfiretimer_p2"
            }),
            EntityTimerStart({
                Name = "et_spitfiretimer_p2"
            }),
            MapFlagSetTrue({
                Name = "mf_usespitfire_p2"
            }),
            PlayerSquadSpawnWithTag({
                Tag = "spitfire_p2",
                TargetTag = "spawn_spitfire_p2",
                Player = "pl_Player2",
                SquadId = 674,
                Direction = 210,
                HealthPercent = default,
                Amount = default
            }),
            SquadGoto({
                Tag = "spitfire_p2",
                TargetTag = "goto_spitfire_p2",
                Run = default,
                Direction = default
            }),
            EntityValueAdd({
                Name = "ev_spitfirecounter_p2",
                Value = 1
            }),
            MapFlagSetTrue({
                Name = "mf_OCspitfirespawn_p2"
            })
        }
    }),
    OnEvent({
        Conditions = {
            SquadIsDead({ For = "ALL", Tag = "spitfire_p2" }),
            EntityValueIsGreater({
                Name = "ev_spitfirecounter_p2",
                Value = 0
            }),
            EntityTimerIsNotRunning({
                Name = "et_spitfirerespawntimer_p2"
            }),
            EntityTimerIsElapsed({
                Name = "et_spitfiretimer_p2",
                Seconds = 4
            })
        },
        Actions = {
            EntityTimerStart({
                Name = "et_spitfirerespawntimer_p2"
            })
        }
    }),
    OnEvent({
        Conditions = {
            EntityValueIsGreater({
                Name = "ev_spitfirecounter_p2",
                Value = 0
            }),
            EntityValueIsLess({
                Name = "ev_spitfirecounter_p2",
                Value = 5
            }),
            EntityTimerIsElapsed({
                Name = "et_spitfiretimer_p2",
                Seconds = 20
            }),
            SquadIsDead({ For = "ALL", Tag = "spitfire_p2" }),
            EntityTimerIsElapsed({
                Name = "et_spitfirerespawntimer_p2",
                Seconds = 25
            })
        },
        Actions = {
            EntityTimerStop({
                Name = "et_spitfiretimer_p2"
            }),
            EntityTimerStart({
                Name = "et_spitfiretimer_p2"
            }),
            PlayerSquadSpawnWithTag({
                Tag = "spitfire_p2",
                TargetTag = "spitfirespawn_p2",
                Player = "pl_Player2",
                SquadId = 674,
                Direction = 210,
                HealthPercent = default,
                Amount = default
            }),
            SquadGoto({
                Tag = "spitfire_p2",
                TargetTag = "goto_spitfire_p2",
                Run = default,
                Direction = default
            }),
            EntityValueAdd({
                Name = "ev_spitfirecounter_p2",
                Value = 1
            }),
            MapFlagSetTrue({
                Name = "mf_OCspitfirespawn_p2"
            }),
            EntityTimerStop({
                Name = "et_spitfirerespawntimer_p2"
            })
        }
    }),
    OnOneTimeEvent({
        EventName = "1stSpitfire_p2",
        Conditions = {
            EntityValueIsEqual({
                Name = "ev_spitfirecounter_p2",
                Value = 5
            }),
            SquadIsAlive({ For = "ALL", Tag = "spitfire_p2" })
        },
        Actions = {
            MapFlagSetTrue({
                Name = "mf_OClastspitfirespawn_p2"
            })
        }
    })
})
