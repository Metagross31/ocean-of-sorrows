-- TODO: Fix respawning of Dropships
State({
    StateName = "SPITFIRESPLAYERONE",
    OnOneTimeEvent({
        EventName = "Init",
        Conditions = {},
        Actions = {
            EntityValueSet({
                Name = "ev_spitfirecounter_p1",
                Value = 0
            })
        }
    }),
    OnOneTimeEvent({
        EventName = "1stSpitfire_p1",
        Conditions = {
            MapTimerIsElapsed({
                Name = "mt_1stspitfiretimer_p1",
                Seconds = 13
            })
        },
        Actions = {
            MapTimerStop({
                Name = "mt_1stspitfiretimer_p1"
            }),
            EntityTimerStart({
                Name = "et_spitfiretimer_p1"
            }),
            MapFlagSetTrue({
                Name = "mf_usespitfire_p1"
            }),
            PlayerSquadSpawnWithTag({
                Tag = "spitfire_p1",
                TargetTag = "spawn_spitfire_p1",
                Player = "pl_Player1",
                SquadId = 674,
                Direction = 210,
                HealthPercent = default,
                Amount = default
            }),
            SquadGoto({
                Tag = "spitfire_p1",
                TargetTag = "goto_spitfire_p1",
                Run = default,
                Direction = default
            }),
            EntityValueAdd({
                Name = "ev_spitfirecounter_p1",
                Value = 1
            }),
            MapFlagSetTrue({
                Name = "mf_OCspitfirespawn_p1"
            })
        }
    }),
    OnEvent({
        Conditions = {
            SquadIsDead({ For = "ALL", Tag = "spitfire_p1" }),
            EntityValueIsGreater({
                Name = "ev_spitfirecounter_p1",
                Value = 0
            }),
            EntityTimerIsNotRunning({
                Name = "et_spitfirerespawntimer_p1"
            }),
            EntityTimerIsElapsed({
                Name = "et_spitfiretimer_p1",
                Seconds = 4
            })
        },
        Actions = {
            EntityTimerStart({
                Name = "et_spitfirerespawntimer_p1"
            })
        }
    }),
    OnEvent({
        Conditions = {
            EntityValueIsGreater({
                Name = "ev_spitfirecounter_p1",
                Value = 0
            }),
            EntityValueIsLess({
                Name = "ev_spitfirecounter_p1",
                Value = 5
            }),
            EntityTimerIsElapsed({
                Name = "et_spitfiretimer_p1",
                Seconds = 20
            }),
            SquadIsDead({ For = "ALL", Tag = "spitfire_p1" }),
            EntityTimerIsElapsed({
                Name = "et_spitfirerespawntimer_p1",
                Seconds = 25
            })
        },
        Actions = {
            EntityTimerStop({
                Name = "et_spitfiretimer_p1"
            }),
            EntityTimerStart({
                Name = "et_spitfiretimer_p1"
            }),
            PlayerSquadSpawnWithTag({
                Tag = "spitfire_p1",
                TargetTag = "spitfirespawn_p1",
                Player = "pl_Player1",
                SquadId = 674,
                Direction = 210,
                HealthPercent = default,
                Amount = default
            }),
            SquadGoto({
                Tag = "spitfire_p1",
                TargetTag = "goto_spitfire_p1",
                Run = default,
                Direction = default
            }),
            EntityValueAdd({
                Name = "ev_spitfirecounter_p1",
                Value = 1
            }),
            MapFlagSetTrue({
                Name = "mf_OCspitfirespawn_p1"
            }),
            EntityTimerStop({
                Name = "et_spitfirerespawntimer_p1"
            })
        }
    }),
    OnOneTimeEvent({
        EventName = "1stSpitfire_p1",
        Conditions = {
            EntityValueIsEqual({
                Name = "ev_spitfirecounter_p1",
                Value = 5
            }),
            SquadIsAlive({ For = "ALL", Tag = "spitfire_p1" })
        },
        Actions = {
            MapFlagSetTrue({
                Name = "mf_OClastspitfirespawn_p1"
            })
        }
    })
})
