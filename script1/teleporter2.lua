TeleporterRange = 15

State({
    StateName = "INIT",
    OnOneTimeEvent({
        Conditions = {},
        Actions = {
            BuildingModeSetEnabled({
                Tag = "tp_switch_2"
            }),
            EntityFlagSetTrue({
                Name = "ef_TeleporterIsEnabled"
            })
        }
    }),

    OnEvent({
        Conditions = {
            EntityFlagIsTrue({
                Name = "ef_TeleporterIsEnabled"
            }),
            EntityHasJustCastedSpell({ -- This checks if the ability has been used
                Tag = "tp_switch_2",
                SpellId = 2219
            }),
            TeamSquadOnGroundIsInRange({ -- Checks for range again
                Team = "tm_Team2",
                TargetTag = "tp_start_2",
                Range = TeleporterRange
            })
        },

        Actions = {
            TeamSquadTeleportInRange({ -- Teleports player units
                Tag = "tp_start_2",
                Range = TeleporterRange,
                TargetTag = "tp_target_2",
                Team = "tm_Team2"
            }),
            TeamSquadTeleportInRange({-- Teleports enemy units
                Tag = "tp_start_2",
                Range = TeleporterRange,
                TargetTag = "tp_target_2",
                Team = "tm_Team1"
            })
        }
    }),

    OnEvent({
        Conditions = {
            EntityFlagIsTrue({
                Name = "ef_TeleporterIsEnabled"
            }),
            EntityHasJustCastedSpell({
                Tag = "tp_switch_2",
                SpellId = 2219
            }),
            TeamSquadIsNotInRange({
                Team = "tm_Team2",
                TargetTag = "tp_start_2",
                Range = TeleporterRange
            })
        },
        Actions = {
            TeamSquadTeleportInRange({
                Tag = "tp_start_2",
                Range = TeleporterRange,
                TargetTag = "tp_target_2",
                Team = "tm_Team1"
            })
        }
    })
})
