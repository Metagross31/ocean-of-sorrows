BattleshipRange = 20

State({
    StateName = "BATTLESHIPS",

    OnIntervalEvent({
        Seconds = 1,
        Conditions = {
            PlayerSquadIsInRange({ -- Checks if units are in range
                Player = "pl_Player1",
                TargetTag = "spawnpoint_battleship_sw",
                Range = BattleshipRange
            }),
            EntityFlagIsFalse({
                Name = "ef_spawner_sw_belongs_to_p2"
            })
        },
        Actions = { -- Gives ownership of activator to player
            EntityTeamSet({
                Tag = "spawn_battleship_sw",
                Team = "tm_Team1"
            }),
            EntityPlayerSet({
                Tag = "spawn_battleship_sw",
                Player = "pl_Player1"
            }),
            EntityFlagSetTrue({
                Name = "ef_spawner_sw_belongs_to_p1"
            })
        }
    }),
    OnIntervalEvent({
        Seconds = 1,
        Conditions = {
            PlayerSquadIsInRange({ -- Checks if units are in range
                Player = "pl_Player2",
                TargetTag = "spawnpoint_battleship_sw",
                Range = 20
            }),
            EntityFlagIsFalse({
                Name = "ef_spawner_sw_belongs_to_p1"
            })
        },
        Actions = { -- Gives ownership of activator to player
            EntityTeamSet({
                Tag = "spawn_battleship_sw",
                Team = "tm_Team2"
            }),
            EntityPlayerSet({
                Tag = "spawn_battleship_sw",
                Player = "pl_Player2"
            }),
            EntityFlagSetTrue({
                Name = "ef_spawner_sw_belongs_to_p2"
            })
        }
    }),

    OnIntervalEvent({ -- Resets ownership to neutral
        Seconds = 1,
        Conditions = {
            PlayerSquadIsNotInRange({
                Player = "pl_Player1",
                TargetTag = "spawn_battleship_sw",
                Range = 25
            }),
            PlayerSquadIsNotInRange({
                Player = "pl_Player2",
                TargetTag = "spawn_battleship_sw",
                Range = 25
            })
        },
        Actions = {
            EntityTeamSet({
                Tag = "spawn_battleship_sw",
                Team = "tm_Neutral"
            }),
            EntityPlayerSet({
                Tag = "spawn_battleship_sw",
                Player = "pl_Neutral"
            }),
            EntityFlagSetFalse({
                Name = "ef_spawner_sw_belongs_to_p1"
            }),
            EntityFlagSetFalse({
                Name = "ef_spawner_sw_belongs_to_p2"
            })
        }
    }),

    OnOneTimeEvent({
        EventName = "neBattleShip_p1",
        Conditions = {
            EntityFlagIsTrue({
                Name = "ef_spawner_sw_belongs_to_p1"
            }),
            EntityHasJustCastedSpell({ -- This checks if the ability has been used
                Tag = "spawn_battleship_sw",
                SpellId = 1931
            }),
            TeamSquadIsInRange({ -- Checks for range again
                Team = "tm_Team1",
                TargetTag = "spawn_battleship_sw",
                Range = BattleshipRange
            })
        },
        Actions {
            PlayerSquadSpawnWithTag({
                Tag = "battleship_sw_p1",
                TargetTag = "spawnpoint_battleship_sw",
                Player = "pl_Player1",
                SquadId = 312,
                Direction = 210,
                HealthPercent = default,
                Amount = default
            })
        }
    }),
    OnOneTimeEvent({
        EventName = "neBattleShip_p2",
        Conditions = {
            EntityFlagIsTrue({
                Name = "ef_spawner_sw_belongs_to_p2"
            }),
            EntityHasJustCastedSpell({ -- This checks if the ability has been used
                Tag = "spawn_battleship_sw",
                SpellId = 1931
            }),
            TeamSquadIsInRange({ -- Checks for range again
                Team = "tm_Team2",
                TargetTag = "spawn_battleship_sw",
                Range = BattleshipRange
            })
        },
        Actions {
            PlayerSquadSpawnWithTag({
                Tag = "battleship_sw_p2",
                TargetTag = "spawnpoint_battleship_sw",
                Player = "pl_Player2",
                SquadId = 312,
                Direction = 210,
                HealthPercent = default,
                Amount = default
            })
        }
    })

})